<?php

namespace Dtn\RegionCheckout\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    protected $_regionFactory;

    public function __construct(\Magento\Directory\Model\RegionFactory $regionFactory)
    {
        $this->_regionFactory = $regionFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $datas = [
            [
                'country_id' => 'NG',
                'code' => 'Abuja FCT',
                'default_name' => 'Abuja FCT',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Abia',
                'default_name' => 'Abia',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Adamawa',
                'default_name' => 'Adamawa',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Akwa Ibom',
                'default_name' => 'Akwa Ibom',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Anambra',
                'default_name' => 'Anambra',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Bauchi',
                'default_name' => 'Bauchi',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Bayelsa',
                'default_name' => 'Bayelsa',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Benue',
                'default_name' => 'Benue',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Borno',
                'default_name' => 'Borno',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Cross River',
                'default_name' => 'Cross River',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Delta',
                'default_name' => 'Delta',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Ebonyi',
                'default_name' => 'Ebonyi',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Edo',
                'default_name' => 'Edo',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Ekiti',
                'default_name' => 'Ekiti',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Enugu',
                'default_name' => 'Enugu',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Gombe',
                'default_name' => 'Gombe',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Imo',
                'default_name' => 'Imo',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Jigawa',
                'default_name' => 'Jigawa',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Kaduna',
                'default_name' => 'Kaduna',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Katisina',
                'default_name' => 'Katisina',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Kebbi',
                'default_name' => 'Kebbi',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Kogi',
                'default_name' => 'Kogi',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Kwara',
                'default_name' => 'Kwara',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Lagos Island & Lekki',
                'default_name' => 'Lagos Island & Lekki',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Lagos Mainland',
                'default_name' => 'Lagos Mainland',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Nasarawa',
                'default_name' => 'Nasarawa',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Niger',
                'default_name' => 'Niger',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Ogun',
                'default_name' => 'Ogun',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Ondo',
                'default_name' => 'Ondo',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Osun',
                'default_name' => 'Osun',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Oyo',
                'default_name' => 'Oyo',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Plateau',
                'default_name' => 'Plateau',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Rivers',
                'default_name' => 'Rivers',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Sokoto',
                'default_name' => 'Sokoto',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Taraba',
                'default_name' => 'Taraba',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Yobe',
                'default_name' => 'Yobe',
            ],
            [
                'country_id' => 'NG',
                'code' => 'Zamfara',
                'default_name' => 'Zamfara',
            ],
        ];

        foreach ($datas as $data) {
            $region = $this->_regionFactory->create();
            $region->addData($data)->save();
        }
    }
}
