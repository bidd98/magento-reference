<?php

namespace Dtn\RegionCheckout\Plugin\StateFilter;

class StateFilter
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;
    protected $allowedNgStates;
    /**
     * StateFilter constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     */
    public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }
    public function afterToOptionArray($subject, $options)
    {
        $allowedStates = $this->scopeConfig->getValue('checkout/state_filter/ng_state_filter', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $this->allowedNgStates = explode(",", $allowedStates);
        $result = array_filter($options, function ($option) {
            if (isset($option['label'])) {
                return in_array($option['label'], $this->allowedNgStates);
            }
            return true;
        });
        if (count($result) > 0) {
            array_unshift(
                $result,
                ['title' => '', 'value' => '', 'label' => __('Please select a region, state or province.')]
            );
        }
        return $result;
    }
}
